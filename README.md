# O Buscador

Este buscador foi desenvolvido como um teste de Frontend para a Tegra.

## Instalação

Use o git para clonar o repositório:

```bash
git clone https://pradoa@bitbucket.org/pradoa/flights-search.git
```

## Executando o projeto

Em um terminal (ou cmd), ir até a pasta do projeto e executar os seguintes comandos em ordem:

```bash
yarn & yarn start
```

Caso a página não abra automaticamente, acessar pelo navegador:

```url
http://localhost:3000/
```

## Packages

Estes foram os principais elementos utilizados:

* [Bootstrap](https://getbootstrap.com/)
* [Ant D](https://ant.design/)
* [TypeScript](https://www.typescriptlang.org/)
* [Axios](https://github.com/axios/axios)
* [Moment.js](https://momentjs.com/)