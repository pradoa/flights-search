import React from 'react';

export default class Global {
    static App: any = null;

    static overridePrototypes() {

        // React
        // Component
        React.Component.prototype.setLoadState = function (element: string) {
            if (!this.state.loading) {
                let arr: Array<string> = [];
                arr[element] = true;
                this.setState({ loading: arr });
            } else {
                let loading = this.state.loading;
                loading[element] = true;
                this.setState({ loading });
            }
        };

        React.Component.prototype.unsetLoadState = function (element: string) {
            if (!this.state.loading) {
                let arr: Array<string> = [];
                arr[element] = false;
                this.setState({ loading: arr });
            } else {
                let loading = this.state.loading;
                loading[element] = false;
                this.setState({ loading });
            }
        };

        React.Component.prototype.onLoadState = function (element: string) {
            if (!this.state.loading)
                return false;
            return this.state.loading[element] === true;
        };
    }
}