import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT
});

export const get = (url: string, params: object) => axiosInstance.get(url, params);
export const post = (url: string, data: object, params: object) => axiosInstance.post(url, data, params);
export const put = (url: string, data: object, params: object) => axiosInstance.put(url, data, params);
export const remove = (url: string, params: object) => axiosInstance.delete(url, params);