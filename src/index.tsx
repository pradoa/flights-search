import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ConfigProvider from 'antd/lib/locale-provider';
import language from 'antd/lib/locale-provider/pt_BR';
import moment from 'moment';
moment.locale('pt-BR');

require('dotenv').config();

import Global from './helpers/global';
Global.overridePrototypes();

import Routes from 'pages/routes';

import './assets/styles/style.css';

class App extends Component {
  constructor(props: any) {
    super(props);
    Global.App = this;
  }

  render(): React.ReactNode {
    return (
      <ConfigProvider locale={language}>
        <Routes />
      </ConfigProvider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement);
