import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

// global styles
import 'antd/dist/antd.css';
import 'assets/styles/style.css';

// pages
import Home from 'pages/Home';

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Home} />
            </Switch>
        </BrowserRouter>
    );
};

export default Routes;
