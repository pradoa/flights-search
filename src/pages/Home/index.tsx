import React, { Component, Fragment } from 'react';
import { Card, Row, Col, AutoComplete, DatePicker, Button, Form, message, Table } from 'antd';
const { Option } = AutoComplete;
import moment, { Moment } from 'moment';
import { get, post } from 'services/api';
import Title from 'antd/lib/typography/Title';
import { toMoney } from 'helpers/functions';

interface IProps {
}

interface IStates {
    valueFrom: string;
    valueTo: string;
    valueDate: string;
    companies: any[];
    allCompanies: any[];
    flightData: any[];
}

export default class Home extends Component<IProps, IStates> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            valueFrom: '',
            valueTo: '',
            valueDate: '',
            companies: [],
            allCompanies: [],
            flightData: []
        };

        this.onSelectFrom = this.onSelectFrom.bind(this);
        this.onSelectTo = this.onSelectTo.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onChangeFrom = this.onChangeFrom.bind(this);
        this.onChangeTo = this.onChangeTo.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.searchFlights = this.searchFlights.bind(this);
        this.renderCompanySuggestion = this.renderCompanySuggestion.bind(this);
    }

    componentWillMount() {
        this.loadCompanies();
    }

    async loadCompanies() {
        let req = await get('flight/companies', {});
        if (req && req.data) {
            this.setState({
                companies: req.data,
                allCompanies: req.data,
            });
        }
    }

    async searchFlights() {
        if (!this.onLoadState('flights')) {
            this.setLoadState('flights');

            const { valueFrom: from, valueTo: to, valueDate: date } = this.state;
            const data = {
                from,
                to,
                date
            };

            if (!from || !to || !date) {
                message.warning('Você precisa selecionar o local e data de partida e o destino!');
            } else {
                let req = await post('flight', data, {});
                if (req && req.data) {
                    this.setState({
                        flightData: req.data
                    });

                    if (req.data.length < 1)
                        message.info('A busca não retornou nenhum resultado!');
                }
            }

            this.unsetLoadState('flights');
        }
    }

    onSelectFrom(valueFrom: string, value2: any) {
        this.setState({ valueFrom });
    }

    onSelectTo(valueTo: string, value2: any) {
        this.setState({ valueTo });
    }

    onSearch(searchText: string) {
        const { allCompanies } = this.state;

        this.setState({
            companies: !searchText ? allCompanies : allCompanies.filter((value: any) => value.nome.toLowerCase().includes(searchText.toLowerCase())),
        });
    }

    onChangeFrom(valueFrom: string) {
        this.setState({ valueFrom });
    }

    onChangeTo(valueTo: string) {
        this.setState({ valueTo });
    }

    onChangeDate(value: Moment) {
        const valueDate = value.format('YYYY-MM-DD');
        this.setState({ valueDate });
    }

    disabledDate(current: Moment) {
        return (current && current < moment('2019-02-09').endOf('day')) || (current && current > moment('2019-02-18').endOf('day'));
    }

    renderCompanySuggestion(item: any) {
        return (<Option key={item.nome} value={item.aeroporto}>{item.nome}</Option>);
    }

    renderForm() {
        const { companies, valueFrom, valueTo } = this.state;

        return (
            <Card title="Buscar Voos" style={{ width: 800, maxWidth: '90%', marginTop: 15, marginBottom: 15 }}>
                <Form>
                    <Row gutter={12}>
                        <Col xs={24} lg={12}>
                            <Form.Item>
                                <AutoComplete
                                    value={valueFrom}
                                    dataSource={companies.map(this.renderCompanySuggestion)}
                                    style={{}}
                                    onSelect={this.onSelectFrom}
                                    onSearch={this.onSearch}
                                    onChange={this.onChangeFrom}
                                    placeholder="Origem"
                                    dropdownMatchSelectWidth={false}
                                    backfill={true}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} lg={12}>
                            <Form.Item>
                                <AutoComplete
                                    value={valueTo}
                                    dataSource={companies.map(this.renderCompanySuggestion)}
                                    style={{}}
                                    onSelect={this.onSelectTo}
                                    onSearch={this.onSearch}
                                    onChange={this.onChangeTo}
                                    placeholder="Destino"
                                    dropdownMatchSelectWidth={false}
                                    backfill={true}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={12}>
                        <Col xs={24} lg={12}>
                            <Form.Item>
                                <DatePicker
                                    placeholder="Data de Partida"
                                    format="DD/MM/YYYY"
                                    disabledDate={this.disabledDate}
                                    defaultPickerValue={moment('2019-02-10')}
                                    style={{ width: '100%' }}
                                    onChange={this.onChangeDate}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} lg={12}>
                            <Form.Item>
                                <Button type="primary" block loading={this.onLoadState('flights')} onClick={this.searchFlights}>Buscar</Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Card>
        );
    }

    renderFlightSubtable(dataSource: any[]) {
        const data: any[] = [];
        dataSource.map((line, i) => {
            line.key = i;
            line.valorTxt = `R$ ${toMoney(line.valor)}`;

            data.push(line);
        });

        return (
            <Table
                className="flightsSub"
                columns={[
                    { title: 'Voo', dataIndex: 'voo', key: 'name' },
                    {
                        title: 'Origem',
                        dataIndex: 'origem',
                        key: 'origem',
                    },
                    {
                        title: 'Destino',
                        dataIndex: 'destino',
                        key: 'destino',
                    },
                    {
                        title: 'Data',
                        dataIndex: 'data_saida',
                        key: 'data',
                    },
                    {
                        title: 'Saída',
                        dataIndex: 'saida',
                        key: 'saida',
                    },
                    {
                        title: 'Chegada',
                        dataIndex: 'chegada',
                        key: 'chegada',
                    },
                    {
                        title: 'Valor',
                        dataIndex: 'valorTxt',
                        key: 'valor',
                    },
                ]}
                dataSource={data}
                size="middle"
                pagination={false}
            />
        );
    }

    renderFlightTable() {
        const { flightData } = this.state;

        const data: any[] = [];

        flightData.map((f, i) => {
            let name = `${f.origem} - ${f.destino}`;
            let value: any = 0;
            let time = moment.duration(0);

            f.voos.map((v: any, i: number) => {
                let datePattern = 'YYYY-MM-DD HH:mm';

                let end = moment(`${f.date} ${v.chegada}`, datePattern);
                let start = moment(`${f.date} ${v.saida}`, datePattern);
                let timeDiff = moment.duration(end.diff(start));
                time.add(timeDiff);
                value += v.valor;
                v.key = i;

                return 0;
            });

            console.log(value);
            value = `R$ ${toMoney(value)}`;

            data.push({
                key: i,
                name,
                value,
                time: `${time.hours()} horas`,
                subs: f.voos
            });
            return 0;
        });

        if (data.length < 1)
            return (null);

        return (
            <Fragment>
                <Title level={3}>Resultados da Busca</Title>
                <Table
                    columns={[
                        { title: 'Name', dataIndex: 'name', key: 'name' },
                        {
                            title: 'Preço Total',
                            dataIndex: 'value',
                            key: 'value',
                            defaultSortOrder: 'descend',
                            sortDirections: ['descend', 'ascend'],
                            sorter: (a, b) => a.value - b.value,
                        },
                        {
                            title: 'Tempo Total de Voo',
                            dataIndex: 'time',
                            key: 'time',
                            defaultSortOrder: 'descend',
                            sortDirections: ['descend', 'ascend'],
                            sorter: (a, b) => a.time.localeCompare(b.time),
                        },
                    ]}
                    expandedRowRender={record => {
                        return this.renderFlightSubtable(record.subs);
                    }}
                    dataSource={data}
                    style={{ width: 800, maxWidth: '90%', marginTop: 15, marginBottom: 15 }}
                    bordered={true}
                />
            </Fragment>
        );
    }

    public render(): React.ReactNode {

        return (
            <div className="contentWrapper d-flex justify-content-center align-items-center flex-column">
                <div className="company-logo">
                    <svg version="1.1" id="Capa_1" x="0px" y="0px"
                        width="360.031px" height="360.031px" viewBox="0 0 360.031 360.031">
                        <g>
                            <g>
                                <g>
                                    <path d="M356.354,82.579l-0.774-0.688c-6.684-4.678-16.201-7.14-27.508-7.14c-17.973,0-36.587,6.095-44.982,11.812l-66.16,45.171
                                        L87.958,159.128c-1.129,0.246-2.177,0.688-3.134,1.339c-2.504,1.735-3.948,4.687-3.783,7.719
                                        c0.258,4.354,3.585,7.815,7.878,8.214l54.542,5.492l-53.164,36.291c-15.829-7.267-30.856-14.316-44.679-20.934
                                        c-21.422-10.268-35.407-12.075-41.979-5.5c-6.842,6.846-2.384,18.543,0.057,22.109l51.212,66.396
                                        c1.723,2.516,3.702,3.957,6.077,4.39c3.942,0.678,7.365-1.682,10.932-4.21l12.967-8.935l72.037-34.979l-10.542,37.65
                                        c-1.156,4.173,0.862,8.503,4.804,10.322c1.12,0.517,2.381,0.786,3.627,0.786c1.778,0,3.48-0.528,4.963-1.519
                                        c0.958-0.643,1.726-1.447,2.366-2.42l41.22-62.643l25.214-29.501l111.81-76.333c1.712-1.162,3.381-2.234,5.056-3.315
                                        c3.855-2.459,7.5-4.792,10.125-7.44C361.213,96.477,361.537,90.193,356.354,82.579z" />
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>

                {this.renderForm()}
                {this.renderFlightTable()}
            </div>
        );
    }

}
