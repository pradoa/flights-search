declare namespace React {
    interface Component {
        setLoadState: function;
        unsetLoadState: function;
        onLoadState: function;
    }
}